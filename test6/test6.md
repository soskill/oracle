﻿# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

- 学号：202010414130   姓名：朱艺卓

### 实验目的

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

### 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

### 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

### 步骤

#### 1.创建表空间

- 创建两个表空间：SALES_DATA 和 INDEX_DATA，用于存储数据和索引。

```
CREATE TABLESPACE SALES_DATA
DATAFILE 'D:\DEMO\GO\sales_data.dbf'
SIZE 100M
AUTOEXTEND ON
NEXT 10M;

CREATE TABLESPACE INDEX_DATA
DATAFILE ''D:\DEMO\GO\index_data.dbf'
SIZE 50M
AUTOEXTEND ON
NEXT 5M;

```

#### 2.创建表

1.CUSTOMERS

```sql

  CREATE TABLE "SYS"."CUSTOMERS_123" 
   (	"CUSTOMER_ID" NUMBER NOT NULL ENABLE, 
	"CUSTOMER_NAME" VARCHAR2(20 BYTE) NOT NULL ENABLE, 
	"PHONE" VARCHAR2(50 BYTE), 
	"EMAIL" VARCHAR2(100 BYTE), 
	"ADDRESS" VARCHAR2(100 BYTE)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;

   COMMENT ON COLUMN "SYS"."CUSTOMERS_123"."CUSTOMER_ID" IS '客户ID';
   COMMENT ON COLUMN "SYS"."CUSTOMERS_123"."CUSTOMER_NAME" IS '客户姓名';
   COMMENT ON COLUMN "SYS"."CUSTOMERS_123"."PHONE" IS '客户电话号码';
   COMMENT ON COLUMN "SYS"."CUSTOMERS_123"."EMAIL" IS '客户电子邮件';
   COMMENT ON COLUMN "SYS"."CUSTOMERS_123"."ADDRESS" IS '客户地址';


```

![image-20230525152626253](C:\Users\Kk\Desktop\期末\oracle\oracle-main\test6\image-20230525152626253.png)

2.PRODUCTS

```sql

  CREATE TABLE "SYS"."PRODUCTS_123" 
   (	"PRODUCT_ID" NUMBER, 
	"PRODUCT_NAME" VARCHAR2(50 BYTE), 
	"PRICE" NUMBER, 
	"DESCRIPTION" VARCHAR2(100 BYTE)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;

   COMMENT ON COLUMN "SYS"."PRODUCTS_123"."PRODUCT_ID" IS '商品ID';
   COMMENT ON COLUMN "SYS"."PRODUCTS_123"."PRODUCT_NAME" IS '商品名称';
   COMMENT ON COLUMN "SYS"."PRODUCTS_123"."PRICE" IS '商品价格';
   COMMENT ON COLUMN "SYS"."PRODUCTS_123"."DESCRIPTION" IS '商品描述';
```

![image-20230525152738243](C:\Users\Kk\Desktop\期末\oracle\oracle-main\test6\image-20230525152738243.png)

3.ORDERS

```sql

  CREATE TABLE "SYS"."ORDERS_123" 
   (	"ORDER_ID" NUMBER(20,0) NOT NULL ENABLE, 
	"CUSTOMER_ID" NUMBER(20,0) NOT NULL ENABLE, 
	"ORDER_DATE" DATE, 
	"TOTAL_AMOUNT" NUMBER(38,0)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;

   COMMENT ON COLUMN "SYS"."ORDERS_123"."ORDER_ID" IS '订单ID';
   COMMENT ON COLUMN "SYS"."ORDERS_123"."CUSTOMER_ID" IS '客户ID';
   COMMENT ON COLUMN "SYS"."ORDERS_123"."ORDER_DATE" IS '订单日期';
   COMMENT ON COLUMN "SYS"."ORDERS_123"."TOTAL_AMOUNT" IS '订单总金额';


```

![image-20230525152928496](C:\Users\Kk\Desktop\期末\oracle\oracle-main\test6\image-20230525152928496.png)

4.ORDER_ITEMS

```sql

  CREATE TABLE "SYS"."ORDER_ITEMS_123" 
   (	"ITEM_ID" NUMBER NOT NULL ENABLE, 
	"ORDER_ID" NUMBER, 
	"PRODUCT_ID" NUMBER, 
	"QUANTITY" NUMBER, 
	"AMOUNT" NUMBER, 
	 CONSTRAINT "ORDER_ITEMS_123_PK" PRIMARY KEY ("ITEM_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;

   COMMENT ON COLUMN "SYS"."ORDER_ITEMS_123"."ITEM_ID" IS '项ID';
   COMMENT ON COLUMN "SYS"."ORDER_ITEMS_123"."ORDER_ID" IS '订单ID';
   COMMENT ON COLUMN "SYS"."ORDER_ITEMS_123"."PRODUCT_ID" IS '商品ID';
   COMMENT ON COLUMN "SYS"."ORDER_ITEMS_123"."QUANTITY" IS '商品数量';
   COMMENT ON COLUMN "SYS"."ORDER_ITEMS_123"."AMOUNT" IS '商品金额';


```

![image-20230525153016682](C:\Users\Kk\Desktop\期末\oracle\oracle-main\test6\image-20230525153016682.png)

![image-20230525153028377](C:\Users\Kk\Desktop\期末\oracle\oracle-main\test6\image-20230525153028377.png)

#### 3.插入数据

1. 插入 CUSTOMERS 表数据:


```sql
INSERT INTO customers_123 (customer_id, customer_name, email, phone, address)
VALUES (1, 'John Smith', 'john@example.com', '1234567890', '123 Main St');
INSERT INTO customers_123 (customer_id, customer_name, email, phone, address)
VALUES (2, 'Jane Doe', 'jane@example.com', '9876543210', '456 Elm St');
-- 插入更多 CUSTOMERS 数据
```

![](C:\Users\Kk\Desktop\期末\oracle\oracle-main\test6\image-20230525153932500.png)

​	2.插入 PRODUCTS 表数据：

```sql
INSERT INTO PRODUCTS (product_id, product_name, price, description)
VALUES (1, 'Product A', 9.99, 'Description of Product A');
INSERT INTO PRODUCTS (product_id, product_name, price, description)
VALUES (2, 'Product B', 19.99, 'Description of Product B');
-- 插入更多 PRODUCTS 数据

```

![image-20230525154141137](C:\Users\Kk\Desktop\期末\oracle\oracle-main\test6\image-20230525154141137.png)

​	3.插入 ORDERS 表数据：

```sql
INSERT INTO ORDERS (order_id, customer_id, order_date, total_amount)
VALUES (1, 1, TO_DATE('2023-05-01', 'YYYY-MM-DD'), 29.98);
INSERT INTO ORDERS (order_id, customer_id, order_date, total_amount)
VALUES (2, 2, TO_DATE('2023-05-02', 'YYYY-MM-DD'), 9.99);
-- 插入更多 ORDERS 数据

```

​	![image-20230525154217056](C:\Users\Kk\Desktop\期末\oracle\oracle-main\test6\image-20230525154217056.png)

4.插入 ORDER_ITEMS 表数据：

```sql
INSERT INTO ORDER_ITEMS (item_id, order_id, product_id, quantity, amount)
VALUES (1, 1, 1, 2, 19.98);
INSERT INTO ORDER_ITEMS (item_id, order_id, product_id, quantity, amount)
VALUES (2, 1, 2, 1, 9.99);
-- 插入更多 ORDER_ITEMS 数据

```

![image-20230525154302152](C:\Users\Kk\Desktop\期末\oracle\oracle-main\test6\image-20230525154302152.png)

#### 4.设计权限及用户分配

设计两个用户：

- SALES_USER：用于执行商品销售相关的操作，包括对所有表的读写权限。
- REPORT_USER：用于生成报表和查询客户信息，只具有对 CUSTOMERS 表的读权限。



为 SALES_USER 授予表的读写权限：

- 打开 SQL Developer 并连接到数据库。

- 使用 SYS 或其他具有管理员权限的用户登录。

- 执行以下 SQL 语句授予 SALES_USER 对表的读写权限：

  ```sql
  GRANT ALL PRIVILEGES ON CUSTOMERS, PRODUCTS, ORDERS, ORDER_ITEMS TO SALES_USER;
  ```

为 REPORT_USER 授予表的只读权限：

- 执行以下 SQL 语句授予 REPORT_USER 对 CUSTOMERS 表的只读权限：

  ```sql
  GRANT SELECT ON CUSTOMERS TO REPORT_USER;
  ```

#### 5.PL/SQL设计

在数据库中建立一个名为 SALES_PACKAGE 的程序包，其中使用 PL/SQL 语言设计存储过程和函数，实现复杂的业务逻辑。

```sql
sqlCopy codeCREATE OR REPLACE PACKAGE SALES_PACKAGE AS
  -- 存储过程：创建新订单
  PROCEDURE CREATE_ORDER(p_customer_id IN NUMBER, p_order_items IN SYS.ODCINUMBERLIST);

  -- 存储过程：根据订单ID计算订单总金额
  PROCEDURE CALCULATE_TOTAL_AMOUNT(p_order_id IN NUMBER);

  -- 函数：根据客户ID获取客户的所有订单
  FUNCTION GET_CUSTOMER_ORDERS(p_customer_id IN NUMBER) RETURN SYS_REFCURSOR;

  -- 函数：根据订单ID获取订单的所有项
  FUNCTION GET_ORDER_ITEMS(p_order_id IN NUMBER) RETURN SYS_REFCURSOR;
END SALES_PACKAGE;
/

CREATE OR REPLACE PACKAGE BODY SALES_PACKAGE AS
  -- 存储过程：创建新订单
  PROCEDURE CREATE_ORDER(p_customer_id IN NUMBER, p_order_items IN SYS.ODCINUMBERLIST) IS
  BEGIN
    -- 在此编写创建新订单的逻辑
    NULL;
  END CREATE_ORDER;

  -- 存储过程：根据订单ID计算订单总金额
  PROCEDURE CALCULATE_TOTAL_AMOUNT(p_order_id IN NUMBER) IS
  BEGIN
    -- 在此编写根据订单ID计算订单总金额的逻辑
    NULL;
  END CALCULATE_TOTAL_AMOUNT;

  -- 函数：根据客户ID获取客户的所有订单
  FUNCTION GET_CUSTOMER_ORDERS(p_customer_id IN NUMBER) RETURN SYS_REFCURSOR IS
    -- 在此声明一个游标变量，用于返回结果集
    cur SYS_REFCURSOR;
  BEGIN
    -- 在此编写根据客户ID获取客户的所有订单的逻辑
    NULL;

    -- 返回结果集
    RETURN cur;
  END GET_CUSTOMER_ORDERS;

  -- 函数：根据订单ID获取订单的所有项
  FUNCTION GET_ORDER_ITEMS(p_order_id IN NUMBER) RETURN SYS_REFCURSOR IS
    -- 在此声明一个游标变量，用于返回结果集
    cur SYS_REFCURSOR;
  BEGIN
    -- 在此编写根据订单ID获取订单的所有项的逻辑
    NULL;

    -- 返回结果集
    RETURN cur;
  END GET_ORDER_ITEMS;
END SALES_PACKAGE;
/
```

#### 6.备份方案

1. 确定备份策略：
   - 定义备份类型：完整备份和增量备份。
   - 确定备份频率：例如每天执行完整备份，每隔一定时间执行增量备份。
   - 确定保留策略：根据可用存储空间和业务需求，确定保留备份的时间段。
2. 配置 RMAN：
   - 安装和配置 Oracle 数据库。
   - 配置 RMAN 与目标数据库的连接。
   - 创建 RMAN 脚本或使用图形界面工具（如 SQL Developer）配置备份任务。
3. 完整备份：
   - 执行完整备份，将整个数据库备份到目标位置。
   - 使用 RMAN 命令或图形界面工具创建完整备份任务。
4. 增量备份：
   - 执行增量备份，备份自上次完整备份以来发生变化的数据块。
   - 使用 RMAN 命令或图形界面工具创建增量备份任务。
5. 存储备份数据：
   - 将备份数据存储在可靠的位置，如本地磁盘、网络存储设备或云存储服务。
   - 考虑数据冗余和灾备需求，可以使用多个备份目标。
6. 定期恢复测试：
   - 定期进行恢复测试以验证备份的有效性和恢复过程的可行性。
   - 恢复测试应涵盖完整备份和增量备份的恢复。
7. 监控和日志记录：
   - 设置合适的监控和警报机制，以确保备份任务正常运行。
   - 记录备份任务的日志和报告，以便进行故障排除和性能分析。
8. 灾难恢复计划：
   - 创建灾难恢复计划，包括恢复时间目标（RTO）和恢复点目标（RPO）等关键指标。
   - 考虑备份数据的远程存储和灾备站点的设置，以确保在灾难情况下能够快速恢复数据。

### 总结

1. 表及表空间设计方案：
   - 创建了两个表空间：SALES_DATA 用于存储数据，INDEX_DATA 用于存储索引。
   - 创建了四张表：CUSTOMERS、PRODUCTS、ORDERS 和 ORDER_ITEMS，分别用于存储客户信息、商品信息、订单信息以及订单项信息。
2. 权限及用户分配方案：
   - 创建了两个用户：SALES_USER 和 REPORT_USER。
   - SALES_USER 用户具有对所有表的读写权限。
   - REPORT_USER 用户只具有对 CUSTOMERS 表的读权限。
3. 程序包设计：
   - 在数据库中建立了一个名为 SALES_PACKAGE 的程序包。
   - 程序包包含了存储过程和函数，用于实现复杂的业务逻辑。
   - 存储过程 CREATE_ORDER 用于创建新订单，接受客户ID和订单项作为参数。
   - 存储过程 CALCULATE_TOTAL_AMOUNT 根据订单ID计算订单的总金额。
   - 函数 GET_CUSTOMER_ORDERS 根据客户ID获取客户的所有订单。
   - 函数 GET_ORDER_ITEMS 根据订单ID获取订单的所有项。
4. 数据库备份方案：
   - 使用 Oracle 的备份和恢复工具 RMAN 进行数据库备份。
   - 配置了定期的完整备份和增量备份。
   - 备份数据存储在可靠的位置，并进行定期的恢复测试以确保备份的有效性。

通过这个数据库设计实验，我们成功地创建了数据库表和表空间，设计了权限和用户分配方案，建立了程序包来实现复杂的业务逻辑，并设计了一套数据库备份方案来保护数据的安全性和可靠性。

这个实验涵盖了数据库设计的不同方面，包括表结构设计、用户权限管理、存储过程和函数设计以及数据库备份策略。它提供了一个基础框架，可以进一步扩展和优化，以满足实际业务需求，并确保数据的完整性和可恢复性。
